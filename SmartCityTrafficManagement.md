---
version: 1
titre: Smart city traffic and noise control
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Serge Ayer
mots-clé: [IoT, Système embarqué, Smart City, LoRa, traffic management, noise control]
mandants: [Projet Fri-Loranet, Service de la mobilité, securaxis]
langue: [F]
confidentialité: non
suite: non
---
## Contexte

Le projet NPR (Nouvelles Politiques Régionales) Fri-LoRanet visant à transformer les villes de Bulle et Fribourg en villes intelligentes a été lancé en décembre de l’année passée. Si le réseau LoRa est déjà opérationnel en ville de Fribourg, il est en développement pour la ville de Bulle.

Le but du réseau LoRa est de contrôler les paramètres importants des deux villes dont l’un est le traﬁc routier. Ce projet de bachelor dédié au traﬁc routier consiste à l’implémentation d’un système de mesure par micro-phone sur le boulevard de Pérolles. La réception des données mesurées se fera au départ par le réseau interne de l’école et ensuite en intégrant une interface LoRa au système de mesure. L’entreprise Securaxis met à dis-position le système de mesure. La collection des données, le stockage et la mise à disposition des données est à développer ainsi que l’intégration du système de communication LoRa.

Comme les capteurs mesurent aussi le niveau sonore. Les valeurs de bruit en décibel devront être stockées avec les données de traﬁc sur un serveur de l’école et être accessibles depuis le web.

La qualité de la détection de traﬁc par microphone sera analysée en comparant les résultats de ces mesures avec celle de caméras installées par la ville.



## Objectifs

1)	installer le système de mesure du traﬁc routier par microphones de Securaxis
2)	rendre ce système accessible par LoRa grâce à un système embarqué intégrant une interface LoRa
3)	développer une plateforme de stockage des données de traﬁc et de bruit et la rendre accessible par internet
4)	planiﬁer l’installation du système sur les axes routiers principaux de la ville de Fribourg

## Contraintes

équipement: capteur de traﬁc Securaxis, le serveur de réseau LoRa de l’école qui doit aussi être utilisé
