---
version: 1
titre: Air quality FriLoRaNet
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Serge Ayer
mots-clé: [IoT, Système embarqué, Smart City, LoRa, Air quality]
mandants: [Projet Fri-Loranet, Service de l'environnement, softcom, TPF]
langue: [F]
confidentialité: non
suite: non


---

```{=tex}
\begin{center}
\includegraphics[width=0.4\textwidth,height=\textheight]{img/decentlab_sensor.jpg}
\end{center}
```


## Contexte

Le projet NPR (Nouvelles Politiques Régionales) Fri-LoRanet visant à transformer les villes de Bulle et Fribourg en villes intelligentes a été lancé en décembre de l’année passée. Si le réseau LoRa est déjà opérationnel en ville de Fribourg, il est en développement pour la ville de Bulle.


Le but du réseau LoRa est de contrôler les paramètres importants des deux villes dont l’un est la qualité de l’air. Ce projet de bachelor dédié à la qualité de l’air sera subdivisé en une partie de préparation et une partie de développement. Dans la première partie un service de mesure simple (par exemple de température) avec stockage des données et accès online sera mis en place. Il faudra pour cela programmer et configurer un serveur d’application LoRa. Cette partie peut se réaliser en remote office mis à part le positionnement/activation d’un capteur LoRa à Fribourg.


La deuxième partie sera dédiée à la véritable qualité de l’air ; un capteur LoRa de NO2 de l’entreprise Decentlab commandé pour le projet sera positionné sur un site du Service de l’Environnement (SEn) et ses valeurs seront comparées aux valeurs de la station du SEn. Le nouveau capteur pourra ainsi être contrôlé et calibré. Il sera ensuite déposé sur le toit d’un véhicule aux côtés d’un gps LoRa et les valeurs des 2 capteurs seront testées en déplaçant le véhicule. Une application sera ensuite développée avec une visualisation géographique et historique de la pollution en ville de Fribourg en collaboration avec l’entreprise softcom. Le but est d’installer ensuite l’équipement sur un bus des TPF pour avoir une visualisation permanente du taux NO2 en ville de Fribourg. Les TPF ont d’ores et déjà donné leur accord et l’intégration sur leur bus se fera en fonction de l’avancée du travail de Bachelor. Une extension du service sur la ville de Bulle (postérieure au projet de Bachelor) est prévue si les tests sont concluants en ville de Fribourg.


## Objectifs

1) récupérer par le réseau LoRa Fribourgeois les données des capteurs de la qualité de l'air (NO2)
2) localiser le capteur utilisation d'un gps couplé au capteur et communiquant vers le centre de réception des données
3) installer les 2 capteurs sur le toit d'un véhicule
4) cartographier la qualité de l'air dans la ville de Fribourg grâce aux données émises par le véhicule à travers Fribourg

## Contraintes

équipement: capteur de polution Decentlab est à comprendre et à utiliser, le serveur de réseau LoRa de l'école doit aussi être utilisé

