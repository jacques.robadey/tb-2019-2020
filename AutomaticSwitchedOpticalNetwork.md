---
version: 1
titre: self Adapting Backbone Network for bandwidth on demand
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
professeurs co-superviseurs:
  - François Buntschu
mots-clé: [management de réseau, configuration de réseau, bande passante à la demande, protection, lambda switching, analyse techno-économique]
attribué à: [Marc Roten]
langue: [F]
confidentialité: non
suite: non
---
## Contexte

Dans la plateforme backbone ciena de l’HEIA-FR (qui correspond à l’ancien réseau national de Netplus), des liens sont protégés de manière automatique en splittant le signal émis sur deux chemins qui se rejoignent sur un switch optique. Ce switch ne laisse passer que le signal primaire, et si ce dernier disparait il switche automatiquement sur le signal de backup. Cette fonctionnalité est intéressante car la rapidité du switching de l’ordre de la ms est imbattable. Les deux chemins sont cependant utilisés en permanence ce qui divise la capacité du réseau par deux.

Une amélioration intelligente a été développée qui n’utilise qu’un seul chemin. Dans le cas d’une perte du canal, l’émetteur est reconfiguré pour changer de longueur d’onde et emprunter le chemin de backup. Cette méthode a l’avantage de ne pas bloquer le lien de backup, mais le changement de couleur (ou longueur d’onde) n’est pas instantané. Durant ce travail de semestre le temps de reconfiguration du lien doit être analysé et jugé s’il est compatible pour différents services : Video ? Transfert de Fichier ou autre.

La fonctionnalité de changement de couleur induisant un changement de direction peut permettre non seulement de rediriger un canal vers un lien de protection mais aussi vers les noeuds nécessitant plus de trafic à certains moments. Ceci correspond au thème principal du travail de bachelor. Il faut reprendre la fonctionnalité destinée à la protection et la coupler à la mesure de trafic sur un lien. Si le trafic dépasse une certaine limite, l’émetteur du lien flexible prend automatiquement la couleur qui l’envoie vers le destinateur dont le lien est surchargé. Le réseau ainsi réalisé devient intelligent et s’adapte automatiquement à la quantité de trafic. L’intelligence du réseau développée durant ce travail de bachelor est particulièrement intéressante pour gérer des liaisons entre datacenters. En effet des liens peuvent être créés/supprimés en fonction des backups en phase de réalisation.
Ce travail de bachelor aura encore l'avantage de pouvoir ajouter un nouveau noeud au système. Comme il y a 4 noeuds en tout, des connections 2 à 2  se reconfigureront en fonction des demandes générales. Toutes les interfaces de tous les noeuds seront ainsi connectées en permanence, ce qui n'est pas le cas actuellement car un noeud est toujours en attente.



## Objectifs

1) Introduire un nouveau noeud dans le réseau national du laboratoire de l'école
2) Tester toutes les configurations de réseau avec les 4 nouveaux liens qui en résultent
3) Mise en place du générateur de trafic SPIRENT pour la création de trafic dynamique entre les 4 noeuds 
4) Amélioration de la génération automatique de nouveaux liens en fonction de la demande et test du fonctionnement du réseau en entier

## Contraintes

équipement: utilisation de la plateforme ciena du labo et du générateur spirent ainsi que de l'équipement en réserve au sein de l'école
